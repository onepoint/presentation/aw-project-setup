# Schematics example

This is an example of schematics

## Testing

To test locally, link the npm package to the target project

```bash
# In this folder
npm link

# Then, in the project folder
npm link aw-project-setup
```

## Unit Testing

`npm test` will run the unit tests, using Jasmine as a runner and test framework.

In VSCode you can use the `.vscode/launch.json` config to run tests with the debugger

## Publishing

```bash
npm run build
npm publish
```
