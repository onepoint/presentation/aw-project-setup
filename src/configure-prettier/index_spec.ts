import * as path from 'path';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Tree } from '@angular-devkit/schematics';
import { JSONFile } from '@schematics/angular/utility/json-file';
import { prettierScriptCheck, prettierScriptFix } from '.';

const schematicName = 'configure-prettier';
describe(schematicName, () => {
  const runner = new SchematicTestRunner('schematics', path.join(__dirname, '../collection.json'));
  let tree: UnitTestTree;

  beforeEach(async () => {
    tree = await runner
      .runExternalSchematicAsync(
        '@schematics/angular',
        'ng-new',
        {
          name: 'test-project',
          version: '0.0.0',
          directory: '.',
        },
        new UnitTestTree(Tree.empty())
      )
      .toPromise();
  });

  it('should add prettier dependencies and configs', async () => {
    await runner.runSchematicAsync(schematicName, {}, tree).toPromise();

    expect(tree.files.indexOf('/.prettierignore')).not.toEqual(-1);
    expect(tree.files.indexOf('/.prettierrc')).not.toEqual(-1);
    expect(tree.readContent('/package.json')).toContain('"prettier');
    expect(tree.readContent('/package.json')).toContain('"tslint-config-prettier');

    const tsLintConf = new JSONFile(tree, 'tslint.json');
    expect(tsLintConf.get(['extends'])).toContain('tslint-config-prettier');

    const packageJson = new JSONFile(tree, 'package.json');
    expect(packageJson.get(['scripts', 'lint'])).toBe(prettierScriptCheck);
    expect(packageJson.get(['scripts', 'lint:fix'])).toBe(prettierScriptFix);
  });
});
