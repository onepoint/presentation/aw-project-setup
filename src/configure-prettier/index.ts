import { mergeWith, Rule, SchematicContext, Tree, url } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks/package-manager/install-task';
import { addPackageJsonDependency, NodeDependencyType } from '@schematics/angular/utility/dependencies';
import { JSONFile } from '@schematics/angular/utility/json-file';

const CHECK_SYMBOL = '\u2713';
const prettierScript = `prettier 'src/**/*.(ts|html|scss|css|json)'`;
export const prettierScriptCheck = `${prettierScript} --check`;
export const prettierScriptFix = `${prettierScript} --write`;

export function configurePrettier(): Rule {
  return (_tree: Tree, _context: SchematicContext): Rule => {
    _context.logger.info('Configure Prettier');

    addPrettierDependencies(_tree, _context);
    _context.addTask(new NodePackageInstallTask());

    const tsConfigSpecJson = new JSONFile(_tree, 'tslint.json');
    const oldExtends = tsConfigSpecJson.get(['extends']) as Array<string>;
    tsConfigSpecJson.modify(['extends'], [...[oldExtends], ...['tslint-config-prettier']]);

    const packageJson = new JSONFile(_tree, 'package.json');
    const oldScripts = packageJson.get(['scripts']) as object;
    packageJson.modify(['scripts'], { ...oldScripts, ...{ lint: prettierScriptCheck, 'lint:fix':  prettierScriptFix} });
    return mergeWith(url('./files'));
  };
}

function addPrettierDependencies(tree: Tree, context: SchematicContext) {
  [
    { type: NodeDependencyType.Dev, version: '2.1.2', name: 'prettier' },
    { type: NodeDependencyType.Dev, version: '4.3.0', name: 'husky' },
    { type: NodeDependencyType.Dev, version: '3.1.0', name: 'pretty-quick' },
    { type: NodeDependencyType.Dev, version: '1.18.0', name: 'tslint-config-prettier' },
  ].forEach((d) => {
    addPackageJsonDependency(tree, d);
    context.logger.info(`${CHECK_SYMBOL} add package "${d.name}"`);
  });
}
