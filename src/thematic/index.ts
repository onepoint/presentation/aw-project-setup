import { strings } from '@angular-devkit/core';
import { apply, mergeWith, Rule, SchematicContext, Tree, url, template, move } from '@angular-devkit/schematics';
import { JSONFile } from '@schematics/angular/utility/json-file';

interface ThematicOptions {
  name: string;
}

export function thematic(_options: ThematicOptions) {
  return (_tree: Tree, _context: SchematicContext): Rule => {
    const angularJson = new JSONFile(_tree, './angular.json');
    const defaultProject = angularJson.get(['defaultProject']) as string;
    const basePath = angularJson.get(['projects', defaultProject, 'sourceRoot']) as string;
    const pathParts = _options.name.split('/');
    const targetFolder = pathParts.length > 1 ? pathParts.slice(0, pathParts.length - 1).join('/') : '';
    return mergeWith(
      apply(url('./files'), [
        template({ ..._options, ...{ name: pathParts[pathParts.length - 1] }, ...strings }),
        move(`${basePath}/app/${targetFolder}`)
      ])
    );
  };
}
