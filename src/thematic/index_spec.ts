import * as path from 'path';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { Tree } from '@angular-devkit/schematics';

const schematicName = 'thematic';
describe(schematicName, () => {
  const runner = new SchematicTestRunner('schematics', path.join(__dirname, '../collection.json'));
  let tree: UnitTestTree;

  beforeEach(async () => {
    tree = await runner
      .runExternalSchematicAsync(
        '@schematics/angular',
        'ng-new',
        { name: 'test-project', version: '0.0.0', directory: '.' },
        new UnitTestTree(Tree.empty())
      )
      .toPromise();
  });

  it('should create component', async () => {
    const nameParam = 'test/this-is-a-wonderful';
    await runner.runSchematicAsync(schematicName, { name: nameParam, themes: ['sport'] }, tree).toPromise();
    const classFileName = `/src/app/${nameParam}.component.ts`;
    const templateFileName = `/src/app/${nameParam}.component.html`;

    expect(tree.files.indexOf(classFileName) !== -1).toBeTrue();
    expect(tree.readContent(classFileName)).toContain('ThisIsAWonderfulComponent');
    expect(tree.readContent(templateFileName)).toContain('<li>Sport</li>');
  });
});
