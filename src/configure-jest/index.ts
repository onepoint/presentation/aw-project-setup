import { mergeWith, Rule, SchematicContext, Tree, url } from '@angular-devkit/schematics';
import { NodePackageInstallTask } from '@angular-devkit/schematics/tasks';
import {
  addPackageJsonDependency,
  NodeDependencyType,
  removePackageJsonDependency,
} from '@schematics/angular/utility/dependencies';
import { JSONFile } from '@schematics/angular/utility/json-file';

const CANCEL_SYMBOL = '\u00D7';
const CHECK_SYMBOL = '\u2713';

export function configureJest(): Rule {
  return (_tree: Tree, _context: SchematicContext): Rule => {
    _context.logger.info('Configure Jest runner');

    addJestDependencies(_tree, _context);
    _context.addTask(new NodePackageInstallTask());
    
    _tree.delete('karma.conf.js');
    _tree.delete('src/test.ts');
    
    const tsConfigSpecJson = new JSONFile(_tree, 'tsconfig.spec.json');
    tsConfigSpecJson.modify(['compilerOptions', 'types'], ['jest']);
    tsConfigSpecJson.modify(['compilerOptions', 'esModuleInterop'], true);
    
    const defaultProject = new JSONFile(_tree, 'angular.json').get(['defaultProject']) as string;
    new JSONFile(_tree, 'angular.json').modify(
        ['projects', defaultProject, 'architect', 'test', 'builder'],
        '@angular-builders/jest:run'
    );
    
    return mergeWith(url('./files'));
  }
}

function addJestDependencies(tree: Tree, context: SchematicContext) {
  // Remove default Karma dependencies
  [
    'karma',
    'karma-chrome-launcher',
    'karma-coverage-istanbul-reporter',
    'karma-jasmine-html-reporter',
    'karma-jasmine',
    'karma-jasmine',
    '@types/jasmine',
    '@types/jasminewd2',
    'jasmine-core',
    'jasmine-spec-reporter',
  ].forEach((d) => {
    removePackageJsonDependency(tree, d);
    context.logger.info(`${CANCEL_SYMBOL} remove package "${d}"`);
  });

  // Add Jest dependencies
  [
    { type: NodeDependencyType.Dev, version: '10.0.1', name: '@angular-builders/jest' },
    { type: NodeDependencyType.Dev, version: '26.0.14', name: '@types/jest' },
    { type: NodeDependencyType.Dev, version: '26.5.2', name: 'jest' },
  ].forEach((d) => {
    addPackageJsonDependency(tree, d);
    context.logger.info(`${CHECK_SYMBOL} add package "${d.name}"`);
  });
}
