import { Tree } from '@angular-devkit/schematics';
import { SchematicTestRunner, UnitTestTree } from '@angular-devkit/schematics/testing';
import { JSONFile } from '@schematics/angular/utility/json-file';
import * as path from 'path';

const schematicName = 'configure-jest';

describe(schematicName, () => {
  const runner = new SchematicTestRunner('schematics', path.join(__dirname, '../collection.json'));
  let tree: UnitTestTree;

  beforeEach(async () => {
    tree = await runner
      .runExternalSchematicAsync(
        '@schematics/angular',
        'ng-new',
        {
          name: 'test-project',
          version: '0.0.0',
          directory: '.',
        },
        new UnitTestTree(Tree.empty())
      )
      .toPromise();
  });

  it('should remove karma and jasmine', async () => {
    await runner.runSchematicAsync(schematicName, {}, tree).toPromise();
    expect(tree.files.indexOf('/karma.conf.js')).toEqual(-1);
    expect(tree.files.indexOf('/src/test.ts')).toEqual(-1);
    expect(tree.readContent('/package.json')).not.toContain('"karma');
    expect(tree.readContent('/package.json')).not.toContain('"jasmine');
    expect(tree.readContent('/package.json')).not.toContain('/jasmine');
  });

  it('should add jest dependencies and config', async () => {
    await runner.runSchematicAsync(schematicName, { }, tree).toPromise();
    expect(tree.readContent('/package.json')).toContain('"@angular-builders/jest":');
    expect(tree.readContent('/package.json')).toContain('"@types/jest":');
    expect(tree.readContent('/package.json')).toContain('"jest":');
    
    const tsConfigSpecJson = new JSONFile(tree, 'tsconfig.spec.json');
    expect(tsConfigSpecJson.get(['compilerOptions', 'types'])).toContain('jest');
    expect(tree.files.indexOf('/jest.config.js')).not.toEqual(-1);
    expect(runner.tasks.findIndex(t => t.name === 'node-package')).not.toEqual(-1);
  });
});
