exports = {
  verbose: true,
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.ts']
};
