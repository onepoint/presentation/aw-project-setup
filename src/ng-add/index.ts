import { chain, Rule, schematic, SchematicContext, Tree } from '@angular-devkit/schematics';

interface ProjectSetupOptions {
  useJestTestRunner: boolean;
  addNpmScripts: boolean;
}

export function projectSetup(_options: ProjectSetupOptions): Rule {
  return () => {
    return chain([
      (_tree: Tree, _context: SchematicContext) => {
        if (_options.useJestTestRunner) {
          return schematic('configure-jest', _options);
        }
      },
      (_tree: Tree, _context: SchematicContext) => {
        if (_options.addNpmScripts) {
          return schematic('configure-prettier', _options);
        }
      },
    ]);
  };
}
